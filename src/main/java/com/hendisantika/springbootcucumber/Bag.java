package com.hendisantika.springbootcucumber;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cucumber
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-23
 * Time: 19:25
 */
public final class Bag {

    private final ArrayList<String> things;

    Bag() {
        things = new ArrayList<>();
    }

    void add(final String something) {
        things.add(something);
    }

    public ArrayList<String> getThings() {
        return things;
    }

    public boolean isEmpty() {
        return things.isEmpty();
    }

    public void removeEverything() {
        things.clear();
    }
}